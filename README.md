# Description

This is Harrow's knowledge base.  Articles are written in Restructured
Text in the [source](./source) directory.  See
[this guide](http://www.sphinx-doc.org/en/stable/rest.html) for an
introduction to Restructured Text.

# Requirements

- Sphinx
- ReadTheDocs Theme for Sphinx

## Installation on Fedora

    sudo dnf install python-sphinx python-pip
    sudo pip install sphinx_rtd_theme

## Installation on Ubuntu

    sudo apt-get install python-sphinx python-pip
    sudo pip install sphinx_rtd_theme

# Workflow

1. Make your changes to knowledge base articles
1. Run `make html` to see the resulting HTML (check the
   [build](./build) directory).
1. If you are happy with the result, commit and push it
1. Pushing to the knowledge base will trigger this job on Harrow:
   https://www.app.harrow.io/#/a/jobs/3088e15a-5143-44b0-3456-4285e796f9b4
1. If the job ran successfully, the new version of the knowledge base
   is available at https://kb.harrow.io.
